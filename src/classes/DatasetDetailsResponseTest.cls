@isTest
public class DatasetDetailsResponseTest {
    
	static testMethod void testParse() {
		String json = '{'+
		'  \"id\": 1004804,'+
		'  \"name\": \"case_routing_intent.csv\",'+
		'  \"createdAt\": \"2017-06-22T19:31:58.000+0000\",'+
		'  \"updatedAt\": \"2017-06-22T19:31:59.000+0000\",'+
		'  \"labelSummary\": {'+
		'    \"labels\": ['+
		'      {'+
		'        \"id\": 23649,'+
		'        \"datasetId\": 1004804,'+
		'        \"name\": \"Order Change\",'+
		'        \"numExamples\": 26'+
		'      },'+
		'      {'+
		'        \"id\": 23650,'+
		'        \"datasetId\": 1004804,'+
		'        \"name\": \"Sales Opportunity\",'+
		'        \"numExamples\": 44'+
		'      },'+
		'      {'+
		'        \"id\": 23651,'+
		'        \"datasetId\": 1004804,'+
		'        \"name\": \"Billing\",'+
		'        \"numExamples\": 24'+
		'      },'+
		'      {'+
		'        \"id\": 23652,'+
		'        \"datasetId\": 1004804,'+
		'        \"name\": \"Shipping Info\",'+
		'        \"numExamples\": 30'+
		'      },'+
		'      {'+
		'        \"id\": 23653,'+
		'        \"datasetId\": 1004804,'+
		'        \"name\": \"Password Help\",'+
		'        \"numExamples\": 26'+
		'      }'+
		'    ]'+
		'  },'+
		'  \"totalExamples\": 150,'+
		'  \"totalLabels\": 5,'+
		'  \"available\": true,'+
		'  \"statusMsg\": \"SUCCEEDED\",'+
		'  \"type\": \"text-intent\",'+
		'  \"object\": \"dataset\"'+
		'}';
		DatasetDetailsResponse r = DatasetDetailsResponse.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		DatasetDetailsResponse.LabelSummary objLabelSummary = new DatasetDetailsResponse.LabelSummary(System.JSON.createParser(json));
		System.assert(objLabelSummary != null);
		System.assert(objLabelSummary.labels == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		DatasetDetailsResponse.Labels objLabels = new DatasetDetailsResponse.Labels(System.JSON.createParser(json));
		System.assert(objLabels != null);
		System.assert(objLabels.id == null);
		System.assert(objLabels.datasetId == null);
		System.assert(objLabels.name == null);
		System.assert(objLabels.numExamples == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		DatasetDetailsResponse objDatasetDetailsResponse = new DatasetDetailsResponse(System.JSON.createParser(json));
		System.assert(objDatasetDetailsResponse != null);
		System.assert(objDatasetDetailsResponse.id == null);
		System.assert(objDatasetDetailsResponse.name == null);
		System.assert(objDatasetDetailsResponse.createdAt == null);
		System.assert(objDatasetDetailsResponse.updatedAt == null);
		System.assert(objDatasetDetailsResponse.labelSummary == null);
		System.assert(objDatasetDetailsResponse.totalExamples == null);
		System.assert(objDatasetDetailsResponse.totalLabels == null);
		System.assert(objDatasetDetailsResponse.available == null);
		System.assert(objDatasetDetailsResponse.statusMsg == null);
		System.assert(objDatasetDetailsResponse.type_Z == null);
		System.assert(objDatasetDetailsResponse.object_Z == null);
        
        DatasetDetailsResponse cntrlr = new DatasetDetailsResponse();
	}

}